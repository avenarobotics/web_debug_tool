<?php
require_once(getcwd() . "/db_input/get_table_data.php");
require_once(getcwd(). "/db_input/json_parser.php");
$db_data = get_table_data("item_element");
?>

<div class='alert alert-info' role='alert'>hover cursor on table cell to display timestamp</div>

<div class="card">
    <div class="card-header">validate estimate shape</div>
    <table class="table table-striped">
        <thead class="thead-dark">
            <th>element label</th>
            <th>id</th>
            <th>estimated position</th>
            <th>GT position</th>
            <th>position error</th>
            <th>estimated orientation</th>
            <th>GT orientation</th>
            <th>orientation error</th>
            <th>estimated size</th>
            <th>GT size</th>
            <th>size error</th>
        </thead>
        <tbody>
            <?php foreach ($db_data as $key => $value) : ?>
                <tr>
                    <td><?= $db_data[$key]["element_label"] ?></td>
                    <td><?= $db_data[$key]["id"] ?></td>
                    <td><?php displayPosition($db_data[$key]["position"]); ?></td>
                    <td><?php displayPosition(null); ?></td>
                    <td><?php displayPositionError($db_data[$key]["estimation_error"]); ?></td>
                    <td><?php displayOrientation($db_data[$key]["orientation"]); ?></td>
                    <td><?php displayOrientation(null); ?></td>
                    <td><?php displayOrientationError($db_data[$key]["estimation_error"]); ?></td>
                    <td><pre><?= prettyPrint($db_data[$key]["primitive_shape"]); ?></pre></td>
                    <td><pre><?= prettyPrint(null); ?></pre></td>
                    <td><?php displaySizeError($db_data[$key]["estimation_error"], $db_data[$key]["element_label"]); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table> 
</div>