<?php
require_once(getcwd() . "/db_input/get_table_data.php");
$db_data = get_table_data(
    "item_cam2",
    "SELECT
        item_cam2_id,
        scene_id,
        mask, mask_timestamp,
        accuracy, accuracy_timestamp,
        label.label, label_id_timestamp,
        depth_data, depth_data_timestamp,
        pcl_data, pcl_data_timestamp
    FROM 
        avena_db.item_cam2
        LEFT JOIN label ON item_cam2.label_id = label.label_id;"
);
?>
<script src="scripts/set_session_var.js"></script>
<script src="scripts/pnglib.js"></script>
<script src="scripts/decode_rle.js"></script>
<script src="scripts/display_mask.js"></script>

<div class="card">
    <div class="card-header">item_cam2</div>
    <form method="post" id="pointcloud_form" action="/pages/pointcloud.php">
        <input type="hidden" name="table" value="item_cam2" />
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>item_cam2_id</th>
                    <th>scene_id</th>
                    <th class="image-td">mask</th>
                    <th>accuracy</th>
                    <th>label</th>
                    <th class="image-td">depth_data</th>
                    <th>pcl_data</th>
                </tr>
            </thead>
            <?php foreach ($db_data as $key => $value) : ?>
                <tr>
                    <td><?= $db_data[$key]["item_cam2_id"]; ?></td>
                    <td><?= $db_data[$key]["scene_id"]; ?></td>
                    <td class="image-td" title="<?= $db_data[$key]["mask_timestamp"]; ?>">
                        <?php if (!$db_data[$key]["mask"]) : ?>
                            <span class="badge badge-warning">No mask data from this camera in database</span>
                        <?php else : ?>
                            <script>
                                display_mask(<?= $db_data[$key]["mask"] ?>, 180, 320)
                            </script>
                        <?php endif; ?>
                    </td>
                    <td><?= round($db_data[$key]["accuracy"], 4) ?></td>
                    <td><?= $db_data[$key]["label"] ?></td>
                    <td class="image-td" title="<?= $db_data[$key]["depth_data_timestamp"]; ?>">
                        <?php if ($db_data[$key]["depth_data"]) : ?>
                            <img src="data:image/png;base64,<?= base64_encode($db_data[$key]["depth_data"]) ?>" width="320" height="180" />
                        <?php else : ?>
                            <span class="badge badge-warning">No depth data from this camera in database</span>
                        <?php endif; ?>
                    </td>
                    <td title="<?= $db_data[$key]["pcl_data_timestamp"]; ?>"><button type="submit" class="btn btn-primary" name="id" onclick="setColumnName('pcl_data')" value="<?= $db_data[$key]["item_cam2_id"]; ?>">pcl_data</button></td>
                </tr>
            <?php endforeach; ?>
    </form>
</div>