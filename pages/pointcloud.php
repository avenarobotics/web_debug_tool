<?php
    $table=$_POST["table"];
    $column=$_POST["pointcloud"];
    $id= $_POST['id'] ?? null;
?>
<html>
<head>
    <title>DebugToolPointCloud</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="bootstrap-css/bootstrap.css">
</head>
<body>
    <script src="../scripts/three.js"></script>
    <script src="../scripts/decode_pointcloud.js"> </script>
    <script type="x-shader/x-vertex" id="vertexshader">

        attribute float size;
        attribute vec3 customColor;

        varying vec3 vColor;

        void main() {

            vColor = customColor;

            vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );

            gl_PointSize = size * ( 300.0 / -mvPosition.z );

            gl_Position = projectionMatrix * mvPosition;

        }

    </script>
    <script type="x-shader/x-fragment" id="fragmentshader">

        uniform vec3 color;
        uniform sampler2D pointTexture;

        varying vec3 vColor;

        void main() {

            gl_FragColor = vec4( color * vColor, 1.0 );
            gl_FragColor = gl_FragColor * texture2D( pointTexture, gl_PointCoord );

        }

    </script>
    <script src="../scripts/OrbitControls.js"></script>
    <div id="controls">
        <table>
            <tr>
                <td>pointcloud info:</td>
                <td>table=<?= $table ?></td>
                <td>column=<?= $column ?></td>
            </tr>
            <tr>
                <td></td>
                <td><input hidden type="text" id="point_size_tracker" size="3" value="0.005"/></td>
                <td></td>
                <td><input hidden type="range" name="point_size" id="point_size" step="0.001" min="0.001" max="0.01" value="0.005" onchange="textChange(this)" /></td>
            </tr>
            <tr>
                <td>
                    camera_position
                    x:<span id="camera_position_x"></span>
                    y:<span id="camera_position_y"></span>
                    z:<span id="camera_position_z"></span>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>   
        
    </div>
    <div id="camera info">
    </div>
    <script src="../scripts/set_session_var.js"></script>
    <script type="text/javascript">
        function textChange(tBox) {
            document.getElementById("point_size_tracker").value = tBox.value;
        }
        function applyViewerChanges()
        {
            displayPointCloud(point_cloud_data);
        }
        function handlePointCloudData()
        {
            var host = location.hostname;
            console.log("hostname: "+host);
            var url = "http://"+host+":89/db_input/get_pointcloud_data.php?table=<?= $table ?>&column=<?= $column ?><?php if($id!=null){echo "&id=$id";} ?>";
            var column = getColumnName();
            if(column!=null)
                url = "http://"+host+":89/db_input/get_pointcloud_data.php?table=<?= $table ?>&column="+column+"<?php if($id!=null){echo "&id=$id";} ?>";
            var req = new XMLHttpRequest();
            req.open('GET', url, true); 
            req.responseType = "arraybuffer";
            req.onload = function (oEvent) {
                var arrayBuffer = req.response; // Note: not oReq.responseText
                if (arrayBuffer) {
                    var byteArray = new Uint8Array(arrayBuffer);
                    var point_cloud_data = decodePointCloud(byteArray);
                    console.log(point_cloud_data);
                    displayPointCloud(point_cloud_data);
                }
            };
            req.send(null);
        }
    </script>
    <script src="../scripts/display_pointcloud.js"></script>
    <script type="text/javascript">
        handlePointCloudData();
    </script>
</body>
</html>