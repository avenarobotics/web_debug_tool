<?php
require_once(getcwd() . "/db_input/get_table_data.php");
require_once(getcwd() . "/pages/pretty_json.php");
$db_data = get_table_data(
    "item_element",
    "SELECT 
        item_element.item_element_id, item_element.item_id, item_element.element_label, 
        item_element.element_mask_1, item_cam1.accuracy as accuracy1,
        item_element.element_mask_2, item_cam2.accuracy as accuracy2,
        item_element.element_depth_1, item_element.element_depth_2,
        item_element.pcl_merged, item_element.element_pcl_1, item_element.element_pcl_2,
        item_element.primitive_shape, item_element.position, item_element.orientation
    FROM 
        avena_db.item_element
        LEFT JOIN avena_db.item ON item_element.item_id = item.item_id
        LEFT JOIN avena_db.item_cam1 ON  item_cam1.item_cam1_id = item.item_cam1_id
        LEFT JOIN avena_db.item_cam2 ON  item_cam2.item_cam2_id = item.item_cam2_id;"
);
?>

<div class='alert alert-info' role='alert'>hover cursor on table cell to display timestamp</div>

<script src="scripts/set_session_var.js"></script>
<script src="scripts/pnglib.js"></script>
<script src="scripts/decode_rle.js"></script>
<script src="scripts/display_mask.js"></script>

<div class="card">
    <div class="card-header">item_element</div>

    <form method="post" id="pointcloud_form" action="/pages/pointcloud.php">
        <input type="hidden" name="table" value="item_element" />
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>item_element_id</th>
                    <th>item_id</th>
                    <th>element_label</th>
                    <th>pcl_merged</th>
                    <th class="image-td">element_mask_1</th>
                    <th>cam_1_accuracy</th>
                    <th class="image-td">element_mask_2</th>
                    <th>cam_2_accuracy</th>
                    <th class="image-td">element_depth_1</th>
                    <th class="image-td">element_depth_2</th>
                    <th>element_pcl_1</th>
                    <th>element_pcl_2</th>
                    <th>primitive_shape</th>
                    <th>position</th>
                    <th>orientation</th>
                </tr>
            </thead>
            <?php foreach ($db_data as $key => $value) : ?>
                <tr>
                    <td><?= $db_data[$key]["item_element_id"]; ?></td>
                    <td><?= $db_data[$key]["item_id"]; ?></td>
                    <td title="<?= $db_data[$key]["element_label_timestamp"]; ?>"><?= $db_data[$key]["element_label"]; ?></td>
                    <td title="<?= $db_data[$key]["item_element_id_timestamp"]; ?>"><button type="submit" name="id" onclick="setColumnName('pcl_merged')" value="<?= $db_data[$key]["item_element_id"]; ?>">pcl_data</button></td>
                    <td class="image-td" title="<?= $db_data[$key]["element_mask_1_timestamp"]; ?>">
                        <?php if (!$db_data[$key]["element_mask_1"]) : ?>
                            <span class="warning">No mask data from this camera in database</span>
                        <?php else : ?>
                            <script>
                                display_mask(<?= $db_data[$key]["element_mask_1"] ?>, 180, 320)
                            </script>
                        <?php endif; ?>
                    </td>
                    <td><?= $db_data[$key]["accuracy1"] ?></td>
                    <td class="image-td" title="<?= $db_data[$key]["element_mask_2_timestamp"]; ?>">
                        <?php if (!$db_data[$key]["element_mask_2"]) : ?>
                            <span class="warning">No mask data from this camera in database</span>
                        <?php else : ?>
                            <script>
                                display_mask(<?= $db_data[$key]["element_mask_2"] ?>, 180, 320)
                            </script>
                        <?php endif; ?>
                    </td>
                    <td><?= $db_data[$key]["accuracy2"] ?></td>
                    <td title="<?= $db_data[$key]["element_depth_1_timestamp"]; ?>">
                        <?php if ($db_data[$key]["element_depth_1"]) : ?>
                            <img src="data:image/png;base64,<?= base64_encode($db_data[$key]["element_depth_1"]) ?>" width="320" height="180" />
                        <?php else : ?>
                            <span class="warning">No depth data from this camera in database</span>
                        <?php endif; ?>
                    </td>
                    <td title="<?= $db_data[$key]["element_depth_2_timestamp"]; ?>">
                        <?php if ($db_data[$key]["element_depth_2"]) : ?>
                            <img src="data:image/png;base64,<?= base64_encode($db_data[$key]["element_depth_2"]) ?>" width="320" height="180" />
                        <?php else : ?>
                            <span class="warning">No depth data from this camera in database</span>
                        <?php endif; ?>
                    </td>
                    <td title="<?= $db_data[$key]["element_pcl_1_timestamp"]; ?>"><button type="submit" name="id" onclick="setColumnName('element_pcl_1')" value="<?= $db_data[$key]["item_element_id"]; ?>">pcl_data</button></td>
                    <td title="<?= $db_data[$key]["element_pcl_2_timestamp"]; ?>"><button type="submit" name="id" onclick="setColumnName('element_pcl_2')" value="<?= $db_data[$key]["item_element_id"]; ?>">pcl_data</button></td>
                    <td title="<?= $db_data[$key]["primitive_shape_timestamp"]; ?>"><pre><?= prettyPrint($db_data[$key]["primitive_shape"]); ?></pre></td>
                    <td title="<?= $db_data[$key]["position_timestamp"]; ?>"><pre><?= prettyPrint($db_data[$key]["position"]); ?></pre></td>
                    <td title="<?= $db_data[$key]["orienttation_timestamp"]; ?>"><pre><?= prettyPrint($db_data[$key]["orientation"]); ?></pre></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </form>
</div>