<?php
    require_once(getcwd() . "/db_input/get_table_data.php");
    require_once(getcwd() . "/pages/pretty_json.php");
    $db_data = get_table_data(
        "left_grasp",
        "SELECT * FROM avena_db.left_grasp;"
    );
?>

<table class="table table-striped">
    <thead class="thead-dark">
        <tr>
            <th class="text-center">left_grasp_id</th>
            <th class="text-center">item_id</th>
            <th class="text-center">grasp_position</th>
            <th class="text-center">grasp_orientation</th>
            <th class="text-center">grasp_width</th>
            <th class="text-center">purpose_policy</th>
            <th class="text-center">desired_force</th>            
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db_data as $key => $value): ?>
        <tr>
            <td><?= prettyPrint($db_data[$key]["left_grasp_id"]) ?></td>
            <td><?= prettyPrint($db_data[$key]["item_id"]) ?></td>
            <td><?= prettyPrint($db_data[$key]["grasp_position"]) ?></td>
            <td><?= prettyPrint($db_data[$key]["grasp_orientation"]) ?></td>
            <td><?= prettyPrint($db_data[$key]["grasp_width"]) ?></td>
            <td><?= prettyPrint($db_data[$key]["purpose_policy"]) ?></td>
            <td><?= prettyPrint($db_data[$key]["desired_force"]) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>