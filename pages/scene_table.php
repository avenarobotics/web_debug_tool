<?php
require_once(getcwd() . "/db_input/get_table_data.php");
$db_data = get_table_data("scene");
?>
<script>
    sessionStorage.clear();
</script>
<script src="scripts/decode_rle.js"></script>
<script src="scripts/pnglib.js"></script>
<script src="scripts/display_mask.js"></script>

<div class="card">
    <div class="card-header">scene</div>
    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th>scene_id</th>
                <th class="image-td">camera_1_rgb</th>
                <th class="image-td">camera_1_depth</th>
                <th class="image-td">annotated_camera_1</th>
                <th class="image-td">camera_2_rgb</th>
                <th class="image-td">camera_2_depth</th>
                <th class="image-td">annotated_camera_2</th>
                <th>merged_pcl</th>
                <th>filtered_octomap</th>
                <th>ocupancy_grid</th>
            </tr>
        </thead>
        <form method="post" id="pointcloud_form" action="/pages/pointcloud.php">
            <input type="hidden" name="previous_page" value="scene_table" />
            <input type="hidden" name="table" value="scene" />
            <?php foreach ($db_data as $key => $value) : ?>
                <tr>
                    <td><?= $db_data[$key]["scene_id"] ?></td>
                    <td class="image-td"><img src="data:image/png;base64,<?= base64_encode($db_data[$key]["camera_1_rgb"]) ?>" width="320" height="180" /></td>
                    <td class="image-td"><img src="data:image/png;base64,<?= base64_encode($db_data[$key]["camera_1_depth"]) ?>" width="320" height="180" /></td>
                    <td class="image-td"><img src="data:image/png;base64,<?= base64_encode($db_data[$key]["annotated_camera_1"]) ?>" width="320" height="180" /></td>
                    <td class="image-td"><img src="data:image/png;base64,<?= base64_encode($db_data[$key]["camera_2_rgb"]) ?>" width="320" height="180" /></td>
                    <td class="image-td"><img src="data:image/png;base64,<?= base64_encode($db_data[$key]["camera_2_depth"]) ?>" width="320" height="180" /></td>
                    <td class="image-td"><img src="data:image/png;base64,<?= base64_encode($db_data[$key]["annotated_camera_2"]) ?>" width="320" height="180" /></td>
                    <td>
                        <input type="submit" class="btn btn-primary" value="merged_pcl" name="pointcloud" />
                    </td>
                    <td class="image-td">
                        <?php if ($db_data[$key]["occupancy_grid"]) : ?>
                            <script>
                                display_mask(<?= $db_data[$key]["occupancy_grid"] ?>, 180, 320)
                            </script>
                        <?php else : ?>
                            <span class="badge badge-warning">No occupancy_grid in database</span>
                        <?php endif; ?>
                    </td>
                    <td><input type="submit" class="btn btn-primary" value="filtered_octomap" name="pointcloud" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <input type="submit" class="btn btn-primary" value="pcl_camera_1" name="pointcloud" />
                    </td>
                    <td></td>
                    <td></td>
                    <td>
                        <input type="submit" class="btn btn-primary" value="pcl_camera_2" name="pointcloud" />
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?= $db_data[$key]["camera_1_rgb_timestamp"] ?></td>
                    <td><?= $db_data[$key]["camera_1_depth_timestamp"] ?></td>
                    <td><?= $db_data[$key]["camera_2_rgb_timestamp"] ?></td>
                    <td><?= $db_data[$key]["camera_2_depth_timestamp"] ?></td>
                    <td><?= $db_data[$key]["pcl_camera_1_timestamp"] ?></td>
                    <td><?= $db_data[$key]["pcl_camera_2_timestamp"] ?></td>
                    <td><?= $db_data[$key]["merged_pcl_timestamp"] ?></td>
                    <td><?= $db_data[$key]["occupancy_grid_timestamp"] ?></td>
                </tr>
            <?php endforeach; ?>
        </form>
    </table>
</div>