<?php
require_once(getcwd() . "/db_input/get_table_data.php");
$db_data = get_table_data(
    "item_cam1",
    "SELECT
        item_cam1_id,
        scene_id,
        mask, mask_timestamp,
        accuracy, accuracy_timestamp,
        label.label, label_id_timestamp,
        depth_data, depth_data_timestamp,
        pcl_data, pcl_data_timestamp
    FROM 
        avena_db.item_cam1
        LEFT JOIN label ON item_cam1.label_id = label.label_id;"
);
?>
<script src="scripts/set_session_var.js"></script>
<script src="scripts/pnglib.js"></script>
<script src="scripts/decode_rle.js"></script>
<script src="scripts/display_mask.js"></script>

<div class="card">
    <div class="card-header">item_cam1</div>
    <form method="post" id="pointcloud_form" action="/pages/pointcloud.php">
        <input type="hidden" name="table" value="item_cam1" />
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th class="text-center">item_cam1_id</th>
                    <th class="text-center">scene_id</th>
                    <th class="image-td text-center" >mask</th>
                    <th class="text-center">accuracy</th>
                    <th class="text-center">label</th>
                    <th class="image-td text-center">depth_data</th>
                    <th class="text-center">pcl_data</th>
                </tr>
            </thead>
            <?php foreach ($db_data as $key => $value) : ?>
                <tr>
                    <td><?= $db_data[$key]["item_cam1_id"]; ?></td>
                    <td><?= $db_data[$key]["scene_id"]; ?></td>
                    <td class="image-td" title="<?= $db_data[$key]["mask_timestamp"]; ?>">
                        <?php if (!$db_data[$key]["mask"]) : ?>
                            <span class="badge badge-warning">No mask data from this camera in database</span>
                        <?php else : ?>
                            <script>
                                display_mask(<?= $db_data[$key]["mask"] ?>, 180, 320)
                            </script>
                        <?php endif; ?>
                    </td>
                    <td><?= round($db_data[$key]["accuracy"], 4) ?></td>
                    <td><?= $db_data[$key]["label"] ?></td>
                    <td class="image-td" title="<?= $db_data[$key]["depth_data_timestamp"]; ?>">
                        <?php if ($db_data[$key]["depth_data"]) : ?>
                            <img src="data:image/png;base64,<?= base64_encode($db_data[$key]["depth_data"]) ?>" width="320" height="180" />
                        <?php else : ?>
                            <span class="badge badge-warning">No depth data from this camera in database</span>
                        <?php endif; ?>
                    </td>
                    <td title="<?= $db_data[$key]["pcl_data_timestamp"]; ?>"><button type="submit" class="btn btn-primary" name="id" onclick="setColumnName('pcl_data')" value="<?= $db_data[$key]["item_cam1_id"]; ?>">pcl_data</button></td>
                </tr>
            <?php endforeach; ?>
    </form>
</div>