<div class="card">
    <div class="card-header">Choose table to display</div>
    <form method="get">
        <table class="table table-striped">
            <tr>
                <td>raw scene info</td>
                <td>items elements info</td>
                <td>detection results from cam1</td>
                <td>detection results from cam1</td>
                <td>left grasp info</td>
                <td><b>validation</b></td>
                <td><b>dynamic debug</b></td>
            </tr>
            <tr>
                <td><input type="submit" class="btn btn-info" name="page" value="scene" /></td>
                <td><input type="submit" class="btn btn-info" name="page" value="item_element" /></td>
                <td> <input type="submit" class="btn btn-info" name="page" value="item_cam1" /></td>
                <td> <input type="submit" class="btn btn-info" name="page" value="item_cam2" /></td>
                <td> <input type="submit" class="btn btn-info" name="page" value="left_grasp" /></td>
                <td> <input type="submit" class="btn btn-warning" name="page" value="validation" /></td>
                <td> <input type="submit" class="btn btn-primary" name="page" value="dynamic_debug" /></td>
            </tr>
        </table>
    </form>
</div>

<?php

$PAGES_ROOT = getcwd() . "/pages";

$STATIC_PAGES = ["dynamic_debug"];

if (isset($_GET["page"])) {
    if(in_array($_GET['page'], $STATIC_PAGES))
    {
        header('Location: '.$_GET['page'] . ".html");
    }
    require_once($PAGES_ROOT . "/" . $_GET["page"] . "_table.php");
}

?>