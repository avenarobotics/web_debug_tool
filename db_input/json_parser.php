<?php

function displayPosition(?string $position_json)
{
    if ($position_json == null) {
        print_r("null");
        return;
    }
    $json_arr = json_decode($position_json, true);
    print_r("x = " . round($json_arr["x"], 5) . " [m]<br/>");
    print_r("y = " . round($json_arr["y"], 5) . " [m]<br/>");
    print_r("z = " . round($json_arr["z"], 5) . " [m]<br/>");
}

function displayPositionError(?string $error_json)
{
    if ($error_json == null) {
        print_r("null");
        return;
    }
    $json_arr = json_decode($error_json, true);
    print_r("distance = " . round($json_arr["distance"] * 1000, 3) . " [mm]<br/>");
}

function displayOrientation(?string $orientation_string)
{
    if ($orientation_string == null) {
        print_r("null");
        return;
    }

    $json_arr = json_decode($orientation_string, true);
    print_r("Quaternion <hr/>");
    print_r("x = " . round($json_arr["x"], 5) . "<br/>");
    print_r("y = " . round($json_arr["y"], 5) . "<br/>");
    print_r("z = " . round($json_arr["z"], 5) . "<br/>");
    print_r("w = " . round($json_arr["w"], 5) . "<br/>");
}

function displayOrientationError(?string $error_json)
{
    if ($error_json == null) {
        print_r("null");
        return;
    }
    $json_arr = json_decode($error_json, true);
    if (!isset($json_arr["orientation_error"])) {
        print_r("null");
        return;
    }
    print_r("Angle between axes [%] <hr/>");
    $orientation_error = $json_arr["orientation_error"]["percent_of_error"];

    if (isset($orientation_error["x"]))
        print_r("x = " . round($orientation_error["x"], 3) . "<br/>");

    if (isset($orientation_error["z"]))
        print_r("z = " . round($orientation_error["z"], 3) . "<br/>");

    if (isset($orientation_error["y"]))
        print_r("y = " . round($orientation_error["y"], 3) . "<br/>");
}

function displaySizeError(?string $error_json, string $label)
{
    if ($error_json == null) {
        print_r("null");
        return;
    }

    print_r("Difference between sizes <hr/>");

    $json_arr = json_decode($error_json, true);

    if(!isset($json_arr["size"]))
    {
        print_r("null");
        return;
    }

    $size_error = $json_arr["size"]["percent_of_error"];

    switch($label)
    {
        case "orange":
            print_r("radius = " . $size_error . "<br/>");
            break;
        case "lipton":
        case "milk":
            print_r("x = " . $size_error["x"] . "<br/>");
            print_r("y = " . $size_error["y"] . "<br/>");
            print_r("z = " . $size_error["z"] . "<br/>");
            break;
        case "cucumber":
        case "carrot":
            print_r("radius = " . $size_error["radius"] . "<br/>");
            print_r("height = " . $size_error["height"] . "<br/>");
            break;
        case "plate":
        case "bowl":
            print_r("N/A<br/>");
    }
}

function prettyPrint( $json )
{
    $result = '';
    $level = 0;
    $in_quotes = false;
    $in_escape = false;
    $ends_line_level = NULL;
    $json_length = strlen( $json );

    for( $i = 0; $i < $json_length; $i++ ) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if( $ends_line_level !== NULL ) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if ( $in_escape ) {
            $in_escape = false;
        } else if( $char === '"' ) {
            $in_quotes = !$in_quotes;
        } else if( ! $in_quotes ) {
            switch( $char ) {
                case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                case '{': case '[':
                    $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
            }
        } else if ( $char === '\\' ) {
            $in_escape = true;
        }
        if( $new_line_level !== NULL ) {
            $result .= "\n".str_repeat( "\t", $new_line_level );
        }
        $result .= $char.$post;
    }

    return $result;
}
