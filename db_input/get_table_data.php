<?php

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

function get_table_data(string $table_name, string $custom_query=null)
{
    $user = "avena";
    $pass = "avena";

    $db_data = [];

    try {
        $dbh = new PDO('mysql:host=172.18.18.4;dbname=avena_db', $user, $pass);
        echo "<div class='alert alert-success' role='alert'>connected successfully </div>";
        $query = ($custom_query==null)?"SELECT * FROM $table_name":$custom_query;
        $stmt = $dbh->prepare($query);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $db_data[] = $row;
        }
        $stmt = null;
        $dbh = null;
    } catch (PDOException $e) {
        print "error: " . $e->getMessage() . "<br/>";
        die();
    }
    return $db_data;
}

?>