function convertBlock(incomingData) { // incoming data is a UInt8Array
    let buf = new ArrayBuffer(4);
    let view = new DataView(buf);
    let output = new Float32Array(incomingData.length/4);
    let output_idx = 0;
    for (let idx = 0; idx < incomingData.length; idx += 4) {
        view.setUint8(0,incomingData[idx+3]);
        view.setUint8(1,incomingData[idx+2]);
        view.setUint8(2,incomingData[idx+1]);
        view.setUint8(3,incomingData[idx]);
        output[output_idx++] = view.getFloat32();
    }
    // console.log("output");
    // console.log(output);
    return output;
}

function extrudePlaceholders(floatsData)
{
    let positions = new Float32Array(3 * floatsData.length / 4);
    let position_idx = 0;
    for(let i=0; i<floatsData.length; i++)
    {
        if(i%4===3)
            continue;
        positions[position_idx++] = floatsData[i];
    }

    return positions;
}

function decodePointCloud(pointcloud_bytes) {

    let part = "SIZE";
    let spliced_data = {
        'SIZE': [],
        'FORMAT': [],
        'DATA': []
    };
    let bytes_past = 0;
    for (let i = 0; i < pointcloud_bytes.length; i++) {
        if (pointcloud_bytes[i] === 10 && part !== "DATA") {
            switch (part) {
                case 'SIZE':
                    part = "FORMAT";
                    break;
                case 'FORMAT':
                    part = "DATA";
                    bytes_past = i + 1;
                    break;
            }
        }
        else {
            spliced_data[part].push(pointcloud_bytes[i]);
        }
    }

    let pointcloud_size = parseInt(spliced_data["SIZE"].map(function (e) { return String.fromCharCode(e) }).join(''));
    let format = spliced_data["FORMAT"].map(function (e) { return String.fromCharCode(e) }).join('');
    let points_data = new Uint8Array(spliced_data["DATA"]);

    // console.info("pointcloud size: " + pointcloud_size);
    // console.info("pointcloud format: " + format);
    // console.log(points_data);

    let floatData = convertBlock(points_data);
    //console.log(floatData);

    let positions = extrudePlaceholders(floatData);

    let color = new THREE.Color(0xffffff);
    let colors = new Float32Array(pointcloud_size*3);
    let i = 0;
    colors.forEach(
        () => {
            color.toArray(colors, i * 3);
            i++;
        }
    );

    return {"points": positions, "colors": colors};
}