function display_mask(mask_json, display_width, display_height) {
    var output_mask = decode_rle(mask_json);
    var orginal_width = mask_json["size"][1];
    var orginal_height = mask_json["size"][0];
    //console.log(output_mask);

    var p = new PNGlib(orginal_height, orginal_width, 256);
    var background = p.color(0, 0, 0, 255);
    var idx = 0;
    output_mask.forEach(pixel_value => {
        var pixel_row = Math.floor(idx / orginal_height);
        var pixel_col = idx % orginal_height;
        p.buffer[p.index(pixel_col, pixel_row)] = p.color(pixel_value, pixel_value, pixel_value, 255);
        idx++;
    });

    document.write('<img src="data:image/png;base64,' + p.getBase64() + '" width="' + display_width + '" height="' + display_height + '" class="mask" />');
}