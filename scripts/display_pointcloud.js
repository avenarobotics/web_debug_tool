var scene, camera, renderer;
var controls;
var sphere;
var amount;
var point_cloud_data;
var fps_display;
var camera_position_display;
var lastCalledTime;

function initHelpersDisplays() {
    fps_display = document.getElementById("framerate");
    camera_position_display = new Array(3);
    camera_position_display[0] = document.getElementById("camera_position_x");
    camera_position_display[1] = document.getElementById("camera_position_y");
    camera_position_display[2] = document.getElementById("camera_position_z");
}

//var point_cloud = document.getElementById("pointcloud");
//point_cloud.addEventListener("change", readPointCloudData, false);

function random(min, max) {
    return Math.random() * (max - min) + min;
}

function deleteCanvas() {
    var canvas = document.getElementsByTagName("canvas")[0];
    if (canvas != null) {
        canvas.remove();
    }
}

function displayPointCloud(point_cloud_data) {
    if (fps_display == null || camera_position_display == null)
        initHelpersDisplays();
    deleteCanvas();
    initWindow();
    let point_size = parseFloat(document.getElementById("point_size").value);
    setupPointCloud(point_cloud_data["points"], point_cloud_data["colors"], point_size);
    setupAxis(5);
    setupCamera();

    animate();
}

function setupAxis(size) {
    var axesHelper = new THREE.AxesHelper(size);
    scene.add(axesHelper);
}

function readPointCloudData() {
    if (this.files && this.files[0]) {
        var file = this.files[0];
        var reader = new FileReader();
        var verticies_strings = [];
        point_cloud_data = {};
        reader.addEventListener("load", (event) => {
            var result = event.target.result;
            var lines = result.split('\n');
            var varticies_field = false;
            lines.forEach(element => {
                if (varticies_field) {
                    verticies_strings.push(element);
                }
                if (element == "end_header") {
                    //console.log("EndOfHeader");
                    varticies_field = true;
                }
                if (element.startsWith("element vertex")) {
                    let values = element.split(' ');
                    amount = parseInt(values[2]);
                }
            });
        });
        reader.addEventListener('progress', (event) => {
            if (event.loaded && event.total) {
                const percent = (event.loaded / event.total) * 100;
                //console.log(`Progress: ${Math.round(percent)}`);
            }
        });
        reader.addEventListener("loadend", (event) => {
            //console.log("Loaded");
            //console.log(verticies_strings);
            point_cloud_data["points"] = new Float32Array(amount * 3);
            point_cloud_data["colors"] = new Float32Array(amount * 3);
            let vertex = new THREE.Vector3();
            let coords = [];
            let color = new THREE.Color(0xf0f0f0);
            let i = 0;
            //console.log(verticies_strings.length);
            verticies_strings.forEach(
                element => {
                    coords = element.split(" ");
                    vertex.x = parseFloat(coords[0]);
                    vertex.y = parseFloat(coords[1]);
                    vertex.z = parseFloat(coords[2]);
                    vertex.toArray(point_cloud_data["points"], i * 3);
                    color.toArray(point_cloud_data["colors"], i * 3);
                    i++;
                }
            );
            //console.log(point_cloud_data);
            displayPointCloud(point_cloud_data);
        });
        reader.readAsText(file);
    }
}

function initWindow() {
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(75, 1920 / 1080, 0.01, 1000);
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(1920, 1080);
    document.body.appendChild(renderer.domElement);
}

function setupPointCloud(positions, colors, point_size) {
    var amount = positions.length/3;
    //console.log(amount);
    if(amount==0)
    {
        //window.alert("PointCloud is empty!");
        console.log("Point Cloud is empty");
    }
    var sizes = new Float32Array(amount);

    for (var i = 0; i < amount; i++) {
        sizes[i] = point_size;
    }

    var geometry = new THREE.BufferGeometry();
    geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));
    geometry.setAttribute('customColor', new THREE.BufferAttribute(colors, 3));
    geometry.setAttribute('size', new THREE.BufferAttribute(sizes, 1));

    var loc = window.location.href;
    loc = loc.split("/").slice(0,-2).join("/");


    var material = new THREE.ShaderMaterial({

        uniforms: {
            color: { value: new THREE.Color(0xffffff) },
            pointTexture: { value: new THREE.TextureLoader().load(loc + "/point.png") }
        },
        vertexShader: document.getElementById('vertexshader').textContent,
        fragmentShader: document.getElementById('fragmentshader').textContent,

        blending: THREE.AdditiveBlending,
        depthTest: false,
        transparent: true

    });

    sphere = new THREE.Points(geometry, material);
    scene.add(sphere);
}

function setupCamera() {
    camera.position.z = 2;
    controls = new THREE.OrbitControls(camera, renderer.domElement);

    if(camera_position_display[0].textContent != 0)
    {
        camera.position.x = parseFloat(camera_position_display[0].textContent);
        camera.position.y = parseFloat(camera_position_display[1].textContent);
        camera.position.z = parseFloat(camera_position_display[2].textContent);
    }
}

function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    controls.update();
    camera_position_display[0].textContent = camera.position.x.toFixed(3);
    camera_position_display[1].textContent = camera.position.y.toFixed(3);
    camera_position_display[2].textContent = camera.position.z.toFixed(3);
}