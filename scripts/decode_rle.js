function string_to_uint8(s) {
    return new TextEncoder().encode(s);
}

function memcpy(src, srcOffset, dst, dstOffset, length) {
    var i;

    src = src.subarray || src.slice ? src : src.buffer;
    dst = dst.subarray || dst.slice ? dst : dst.buffer;

    src = srcOffset ? src.subarray ?
        src.subarray(srcOffset, length && srcOffset + length) :
        src.slice(srcOffset, length && srcOffset + length) : src;

    if (dst.set) {
        dst.set(src, dstOffset);
    } else {
        for (i = 0; i < src.length; i++) {
            dst[i + dstOffset] = src[i];
        }
    }

    return dst;
}

function decode_rle(mask) {
    var width = mask["size"][1];
    var height = mask["size"][0];

    var mask_counts = mask["counts"];//.replace("\\", "\\\\");

    var m = 0;
    var p = 0;
    var k;
    var x;
    var more;

    var mask_count_uint8 = string_to_uint8(mask_counts);

    var cnts = new Uint32Array(mask_counts.length);

    while (p < mask_counts.length) {
        x = 0;
        k = 0;
        more = 1;
        while (more) {
            let c = mask_count_uint8[p] - 48;
            x |= (c & 0x1f) << 5 * k;
            more = c & 0x20;
            p++;
            k++;
            if (!more && (c & 0x10)) {
                x |= -1 << 5 * k;
            }
        }
        if (m > 2) {
            x += cnts[m - 2];
        }
        cnts[m++] = x;
    }

    cnts = cnts.subarray(0, m-1);

    var output_mask = new Uint8Array(width * height);
    var mask_value = 0;
    var idx = 0;

    cnts.forEach(chunk_size => {
        let temp_mask_batch = new Uint8Array(chunk_size).fill(mask_value);
        mask_value ^= 255;
        memcpy(temp_mask_batch, 0, output_mask, idx, chunk_size);
        idx += chunk_size;
    });

    return output_mask;
}