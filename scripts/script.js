const IMAGE_WIDGET_WIDTH = 800;
const IMAGE_WIDGET_HEIGHT = 450;
const POINTCLOUD_WIDGET_WIDTH = 1280;
const POINTCLOUD_WIDGET_HEIGHT = 720;
const TEXT_FPS = 10;
const IMAGES_FPS = 10;
const POINT_CLOUDS_FPS = 10;

class ImageViewer {
    constructor(url) {
        console.debug("ImageViewer created");
        this.image = document.createElement('img');
        this.image.width = IMAGE_WIDGET_WIDTH;
        this.image.height = IMAGE_WIDGET_HEIGHT;
        this.url = url;
        this.image.src = "dump_data/no_signal.png";
    }

    addDomElement() {
        console.debug("ImageViewer added to DOM");
        document.getElementById("images").appendChild(this.image);
    }

    refresh() {
        console.time("ImageRequest");
        let req = new XMLHttpRequest();
        req.open("GET", this.url, true);
        req.responseType = 'arraybuffer';
        req.parent = this;
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    let uInt8Array = new Uint8Array(this.response);
                    let i = uInt8Array.length;
                    let biStr = new Array(i);
                    while (i--) {
                        biStr[i] = String.fromCharCode(uInt8Array[i]);
                    }
                    let data = biStr.join('');
                    let base64 = window.btoa(data);
                    if (base64.length === 0)
                        return;
                    this.parent.image.src = "data:image/png;base64," + base64;
                }
            }
        }
        req.send(null);
        console.timeEnd("ImageRequest");
    }
}

class PointCloudViewer {
    constructor(url) {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, POINTCLOUD_WIDGET_WIDTH / POINTCLOUD_WIDGET_HEIGHT, 0.001, 1000);
        this.renderer = new THREE.WebGLRenderer({ preserveDrawingBuffer: true });
        this.renderer.setSize(POINTCLOUD_WIDGET_WIDTH, POINTCLOUD_WIDGET_HEIGHT);
        this.url = url;
        this.point_cloud = [];
        this.current_points = null;
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

        this.location = window.location.href.split("/").slice(0, -1).join("/");

        this.material = new THREE.ShaderMaterial({
            uniforms: {
                color: { value: new THREE.Color(0xffffff) },
                pointTexture: { value: new THREE.TextureLoader().load(this.location + "/point.png") }
            },
            vertexShader: document.getElementById('vertex_shader').textContent,
            fragmentShader: document.getElementById('fragment_shader').textContent,

            blending: THREE.NormalBlending,
            depthTest: false,
            transparent: false
        });
    }

    addDomElement() {
        document.getElementById("pcd").appendChild(this.renderer.domElement);
    }

    setPoints() {
        this.scene.remove(this.current_points);
        let amountOfPoints = this.point_cloud["points"].length / 3;
        let sizes = new Float32Array(amountOfPoints);
        for (let i = 0; i < amountOfPoints; i++) {
            sizes[i] = 0.001;
        }
        let geometry = new THREE.BufferGeometry();
        geometry.setAttribute('position', new THREE.BufferAttribute(this.point_cloud["points"], 3));
        geometry.setAttribute('customColor', new THREE.BufferAttribute(this.point_cloud["colors"], 3));
        geometry.setAttribute('size', new THREE.BufferAttribute(sizes, 1));

        this.current_points = new THREE.Points(geometry, this.material);
        this.scene.add(this.current_points);
    }

    addAxis() {
        let axesHelper = new THREE.AxesHelper(5);
        this.scene.add(axesHelper);
    }

    setupCamera() {
        this.camera.position.z = 2;
    }

    animate() {
        requestAnimationFrame(this.animate.bind(this))
        this.renderer.render(this.scene, this.camera);
        this.controls.update();
    }

    refresh() {
        console.time("PointCloudRequest");
        let req = new XMLHttpRequest();
        req.open("GET", this.url, true);
        req.responseType = "arraybuffer";
        req.parent = this;
        req.onload = function () {
            let array_buffer = req.response;
            if (array_buffer) {
                let byte_array = new Uint8Array(array_buffer);
                req.parent.point_cloud = decodePointCloud(byte_array);
                if (req.parent.point_cloud["points"].length !== 0) {
                    req.parent.setPoints();
                }
            }
        }
        req.send(null);
        console.timeEnd("PointCloudRequest");
    }

    render() {
        this.setupCamera();
        this.addAxis();
        this.animate();
    }
}

class TextViewer {
    constructor(description, url, formating_function) {
        this.url = url;
        this.description = description;
        this.text = document.createElement('p');
        this.formating_function = formating_function;
    }

    refresh() {
        console.time("TextRequest");
        let req = new XMLHttpRequest();
        req.open("GET", this.url, true);
        req.parent = this;
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    if (req.responseText !== "")
                        req.parent.text.innerHTML = req.parent.description + req.parent.formating_function(req.responseText);
                }
            }
        }
        req.send(null);
        console.timeEnd("TextRequest");
    }

    addDomElement() {
        console.debug("TextViewer added to DOM")
        document.body.appendChild(this.text);
    }
}

function format_percentage(str)
{
    let f_value = parseFloat(str);
    return Math.floor(f_value * 100).toString() + "%";
}

class App {
    constructor() {
        console.debug("App created");

        this.rgb1 = new ImageViewer("/db_input/get_change_detect.php?field=camera_1_rgb");
        this.rgb2 = new ImageViewer("/db_input/get_change_detect.php?field=camera_2_rgb");
        this.merged_pcl = new PointCloudViewer("/db_input/get_change_detect.php?field=merged_pcl");
        this.merged_pcl_filtered = new PointCloudViewer("/db_input/get_change_detect.php?field=merged_pcl_filtered");
        this.pcl_diff = new PointCloudViewer("/db_input/get_change_detect.php?field=pcl_diff");
        this.change_percentage = new TextViewer("Change percentage: ", "/db_input/get_change_detect.php?field=scene_change_percentage", format_percentage);

        this.rgb1.addDomElement();
        this.rgb2.addDomElement();
        this.merged_pcl.addDomElement();
        this.merged_pcl_filtered.addDomElement();
        this.pcl_diff.addDomElement();
        this.change_percentage.addDomElement();

        setInterval(this.runImages.bind(this), 1000 / IMAGES_FPS);
        setInterval(this.runPointClouds.bind(this), 1000 / POINT_CLOUDS_FPS);
        setInterval(this.runTexts.bind(this), 1000 / TEXT_FPS);

        this.merged_pcl.render();
        this.merged_pcl_filtered.render();
        this.pcl_diff.render();
    }

    runImages() {
        console.time("runImages");
        this.rgb1.refresh();
        this.rgb2.refresh();
        console.timeEnd("runImages");
    }

    runPointClouds() {
        console.time("runPCLs");
        this.merged_pcl.refresh();
        this.merged_pcl_filtered.refresh();
        this.pcl_diff.refresh();
        console.timeEnd("runPCLs");
    }

    runTexts() {
        console.time("runTexts");
        this.change_percentage.refresh();
        console.timeEnd("runTexts");
    }
}

window.onload = function () {
    try {
        let app = new App();
    } catch (e) {
        console.error(e);
    }
}